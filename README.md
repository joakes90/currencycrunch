# Currency Crunch

**Revisiting Currency Crunch.**

I am revisiting this project some many years after it was initially written.
CC was my first project built for public consumption and was later uploaded to bitbucket
in 2015 for archival. This is not intended to be an indication of my current coding
abilities and is here only for sentimental reasons.

If you do decide to look at the code anyway you will find many best practices
broken, **php** mixed with html and the uglyest css you have seen since the 90's.
What you will also find is rudimentary use of a MySQL database and multipul calls
to JSON APIs and at least a basic concern for user security. Don't get me wrong
this is not a good project, but not too bad for a first time out.