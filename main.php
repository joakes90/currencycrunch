<?php
    session_start();
    error_reporting(E_ERROR | E_PARSE);
$con = mysqli_connect('localhost', 'joakes90_stash', 'G0ldB17', 'joakes90_BTC_values');
$query = "select * from users where user =  ". "'".$_SESSION['username']."'";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_array($result, MYSQLI_NUM);

$addr = $row[1];
$weightau = $row[2];
$weightag = $row[3];
$lastusd = $row[4];
$lastbtc = $row[5];
$lastau = $row[6];
$lastag = $row[7];
if ($addr != 0) {
	$btcballance = btcballance($addr);
} else {
	$btcballance = 0;
}
function btcballance($adress){
    $btcurl = 'http://blockchain.info/address/'.$adress.'?format=json';
    $json = file_get_contents($btcurl);
    $obj = json_decode($json);
    $rawballance = str_pad($obj->final_balance, 9, "0", STR_PAD_LEFT);
    $rawballancesub = ".".substr($rawballance, strlen($rawballance) -8, strlen($rawballance));
    $ballance = floatval(substr($rawballance,0, strlen($rawballance) - 8).$rawballancesub);
    return $ballance;
}
 function btcprice() {
    $cburl = 'https://coinbase.com/api/v1/prices/spot_rate';
    $cbjson = file_get_contents($cburl);
    $cbobj = json_decode($cbjson);
    $price = $cbobj->amount;
    return $price;
 }

$auurl ='http://coinabul.com/api.php';
$aujson = file_get_contents($auurl);
$auobj = json_decode($aujson);

class Metal {
    public $spotptice;
    public $price;
    public $percent;
    public $last;

    public function __construct($name, $obj, $oz, $last){
        if ($name == 'gold'){
            $this->spotptice = $obj->Gold->USD;
        }
        elseif($name == 'silver'){
            $this->spotptice = $obj->Silver->USD;
        }
        $this->last = $last;
        $this->price = round($oz * $this->spotptice, 2);
        $this->percent = ($this->price - $this->last) / $this->last * 100;
    }
}

$btcusd = round(btcballance($addr) * btcprice(),2);
$btcinc = round((floatval($btcusd) - floatval($lastbtc))/floatval($lastbtc) * 100 , 3);
$gold = new Metal('gold', $auobj, $weightau, $row[6]);
$silver = new Metal('silver', $auobj, $weightag,$row[7]);
$total = $btcusd + $gold->price + $silver->price;
$totalinc = round(($total - $lastusd) / $lastusd * 100, 3);
?>
<!DOCTYPEHTML>

<html>
	<head>
		<title><?php echo "$".round($total,2); ?></title>
<link type="text/css" rel="stylesheet" href="stylesheet.css" /
</head>
<body bgcolor="#cccccc">
<div id="background">
    <div id="topbar">
        <p>Currency Crunch</p>
    </div>
    <a href="logout.php"><p style="text-align: right">logout</p></a> <a href="newpassword.php"><p style="text-align: right">change pasword</p></a>
    <div id="info">
        <div class="stats">
            <h4>Bitcoin</h4>
            <a href="newaddress.php">change wallet</a>
            <table>
                <tr>
                    <td> <p>Number of Coins</p> </td>
                    <td> <p>Exchange Rate</p> </td>
                    <td> <p>Value in USD</p> </td>
                    <td> <p>Change from last login</p> </td>
                </tr>
                <tr>
                    <td> <p><?php echo "BTC ".$btcballance; ?></p> </td>
                    <td> <p><?php echo "$".btcprice();?></p> </td>
                    <td> <p><?php echo "$".round($btcusd,2);?></p> </td>
                    <td> <p><?php echo $btcinc."%";?></p> </td>
                </tr>
            </table>
        </div>
        <div id="totals">
            <p id="total">Total</p>
            <br>
            <p id="full"> <?php echo "$".round($total, 2);?></p>
            <p id="per"><? echo "This has changed by ".$totalinc."% since your last login";?> </p>
        </div>
        <br>
        <div class="stats">
            <h4>Gold</h4>
            <a href="changegold.php">change weight</a>
            <table>
                <tr>
                    <td> <p>Weight</p> </td>
                    <td> <p>Spot Price</p> </td>
                    <td> <p>Value in USD</p> </td>
                    <td> <p>Change from last login</p> </td>
                </tr>
                <tr>
                    <td> <p><?php echo $weightau."oz";?></p> </td>
                    <td> <p><?php echo "$".round($gold->spotptice, 2);?></p> </td>
                    <td> <p><?php echo "$".$gold->price;?></p> </td>
                    <td> <p><?php echo round($gold->percent,3)."%";?></p> </td>
                </tr>
            </table>

        </div> <br>
        <div class="stats">
            <h4>Silver</h4>
            <a href="changesilver.php">change weight</a>
            <table>
                <tr>
                    <td> <p>Weight</p> </td>
                    <td> <p>Spot Price</p> </td>
                    <td> <p>Value in USD</p> </td>
                    <td> <p>Change from last login</p> </td>
                </tr>
                <tr>
                    <td> <p><?php echo $weightag."oz";?></p> </td>
                    <td> <p><?php echo "$".round($silver->spotptice, 2);?></p> </td>
                    <td> <p><?php echo "$".round($silver->price, 2);?></p> </td>
                    <td> <p><?php echo round($silver->percent,3)."%";?></p> </td>
                </tr>
            </table>

        </div>

    </div>
     
    <a href="bitcoin:17HCWhvCQH5hL8PZW2V4zvNvFTpn2hTSkx"><p style="text-align: center">17HCWhvCQH5hL8PZW2V4zvNvFTpn2hTSkx</p></a>
</div>
</body>
</html>
<?php
function updateLasts($place, $amount, $kind) {
    $con = mysqli_connect('localhost', 'joakes90_stash', 'G0ldB17', 'joakes90_BTC_values');
  /*  $query = "UPDATE users SET '$kind' = '$amount' WHERE btc = '$place'";*/
    mysqli_query($con, "UPDATE users SET $kind = $amount WHERE user = '$place'");
    mysqli_close($con);
}

updateLasts($_SESSION['username'], $btcusd, 'lastbtc');
updateLasts($_SESSION['username'], $gold->price, 'lastgold');
updateLasts($_SESSION['username'], $silver->price, 'lastsilver');
updateLasts($_SESSION['username'], $total, 'last');
?>