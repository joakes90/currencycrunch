import requests
import datetime
import MySQLdb
from datetime import datetime
btc_url = 'http://data.mtgox.com/api/2/BTCUSD/money/ticker'
btcValue = requests.get(btc_url, headers={'Accept': 'application/json'})

au_url = 'http://coinabul.com/api.php'
auValue = requests.get(au_url, headers={'Accept': 'application/json'})


now = datetime.now()
btc =  round(float(btcValue.json()['data']['avg']['value']), 2)
gold = round(float(auValue.json()['Gold']['USD']), 2)
silver = round(float(auValue.json()['Silver']['USD']), 2)

db = MySQLdb.connect(host="localhost", user="root", passwd="B17C01n", db="BTC_values")
cur = db.cursor()

cur.execute("""UPDATE public_data SET isCurrent=0 WHERE isCurrent=1;""")

cur.execute("""INSERT INTO public_data (time, usdbtc, usdau, usdag, isCurrent) VALUES(%s, %s, %s, %s, 1);""", (now, btc, gold, silver))
db.commit()
